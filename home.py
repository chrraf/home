## add home.py in /etc/rc.local to start a boot time.
## sudo /usr/bin/python3 /home/pi/home/home.py > /home/pi/home/home.log 2>&1 &
## 
import lcddriver
from time import *
import time
import datetime
import astral
from dateutil import tz
import serial
from contextlib import closing
from urllib.request import urlopen
import dateutil.parser
import json
from time import localtime, asctime
from os import stat

WEATHER = {
        0: "Soleil  ",
        1: "Nuageux-",
        2: "Voilé   ",
        3: "Nuageux ",
        4: "Nuageux+",
        5: "Couvert ",
        6: "Brouillard",
        7: "Brouillar*",
        10: "Pluie-  ",
        11: "Pluie   ",
        12: "Pluie+  ",
        13: "Pluie-* ",
        14: "Pluie*  ",
        15: "Pluie+* ",
        16: "Bruine  ",
        20: "Neige-  ",
        21: "Neige   ",
        22: "Neige+  ",
        30: "Pluie*  ",  
        31: "Pluie** ",
        32: "Pluie***",
        40: "Averses-",
        41: "Averses ",
        42: "Averses+",
        43: "Averses-",
        44: "Averses ",
        45: "Averses+",
        46: "Averses*"

}

def lcdLine(text,lineSize):
  if len(text) < lineSize:
     for i in range(len(text),lineSize):
       text = text + ' '
  return text

def Forecast(forecast,heure):
     jour = 'X'
     for f in forecast:
      if(dateutil.parser.parse(f['datetime']).strftime('%H:%M') == heure):
        probarain=f['probarain']
        probafrost=f['probafrost']
        probawind70=f['probawind70']
        weather=f['weather']
        if(probarain <= 20 and probafrost == 0 and probawind70 == 0 and weather < 6):
          jour = "O"
        elif(probarain <= 40 and probafrost == 0 and probawind70 == 0 and (weather < 12 or weather == 40 or weather == 41)):
          jour = "T"
        else:
          jour = "N"
     return jour

def dayForecast(jour):
    now = datetime.datetime.today()
    meteo = ""
    try:
      with closing(urlopen('https://api.meteo-concept.com/api/forecast/daily?token=bac00bd62de93dd53dada98a1dfb6d9603ca3621fe3abecdba316ee58b09fbe3&insee=35001')) as f:
       decoded = json.loads(f.read())
       (city,forecast) = (decoded[k] for k in ('city','forecast'))

       if forecast[jour]['weather'] < 47:
          meteo = "{} {}/{}".format(WEATHER[forecast[jour]['weather']],forecast[jour]['tmin'],forecast[jour]['tmax'])
       else:
          meteo = "**{}** {}/{}".format(forecast[jour]['weather'],forecast[jour]['tmin'],forecast[jour]['tmax'])
    except Exception as err: 
       meteo = "ERREUR"
       print(str(err))
    finally:   
      print(meteo)
      return meteo

def Velo():
    try:
       with closing(urlopen('https://api.meteo-concept.com/api/forecast/nextHours?token=bac00bd62de93dd53dada98a1dfb6d9603ca3621fe3abecdba316ee58b09fbe3&insee=35001&hourly=true')) as f:
         forecast = json.loads(f.read())['forecast']
         jour1 = Forecast(forecast,"08:00")
         jour2 = Forecast(forecast,"12:00")
         jour3 = Forecast(forecast,"14:00")
         jour4 = Forecast(forecast,"18:00")
         velo = 'VELO: '+jour1+' '+jour2+' '+jour3+' '+jour4+'  '
    except Exception as err:
       velo = "ERREUR"
       print(str(err))
    finally:
       print(velo)
       return velo

def lireFichier (emplacement) :
    # Ouverture du fichier contenant la temperature
    fichTemp = open(emplacement)
    # Lecture du fichier
    contenu = fichTemp.read()
    # Fermeture du fichier apres qu'il ai ete lu
    fichTemp.close()
    return contenu

def recupTemp (contenuFich) :
    # Supprimer la premiere ligne qui est inutile
    secondeLigne = contenuFich.split("\n")[1]
    temperatureData = secondeLigne.split(" ")[9]
    # Supprimer le "t="
    temperature = float(temperatureData[2:])
    # Mettre un chiffre apres la virgule
    temperature = temperature / 1000
    return temperature

def lireHumTemp():
    command_encode = str.encode("TEMP\n")
    serialArduino.write(command_encode)
    #Humidite:56.20:Temperature:22.90:Indice de temperature:22.71:
    #1234567890123456789012345678901234678901234567890123456789012
    #         1         2         3        4         5         6
    message = serialArduino.readline()
    return message

def On(code):
    command_encode = str.encode(code)
    serialArduino.write(command_encode)
    message = serialArduino.readline()
    time.sleep(4)
    return message

def Off(code):
    command_encode = str.encode(code)
    serialArduino.write(command_encode)
    message = serialArduino.readline()
    time.sleep(4)
    return message

def testWeb(dateFile,nomFile,command):
    infos = stat(nomFile)
    dateFichier =  asctime(localtime(infos[8]))
    
    if(dateFichier > dateFile):
       dateFile = dateFichier
       mon_fichier = open(nomFile, "r")
       status = mon_fichier.read()
       mon_fichier.close()
       print(status + "\n")
       if(status == "ON"):
          print("lumiere est a allumer")
          On(str(command)+"ON\n")
       if(status == "OFF"):
          print("lumiere est a eteindre")
          Off(str(command)+"OFF\n")
    return dateFile  

fichierLumiere = "/var/www/html/myonoffswitch1"
fichierVolet = "/var/www/html/myonoffswitch2"
infos = stat(fichierLumiere)
dateFLumiere =  asctime(localtime(infos[8]))
infos = stat(fichierVolet)
dateFVolet =  asctime(localtime(infos[8]))

LUMIERE = False
VOLET_HAUT = True
VOLET_SALON = True
VOLET_CHAMBRE = True

loc = astral.Location(('Rennes', 'France', 48.1333, -1.5333, 'Europe/Paris', 40))
jour = False
heure_chambre =  datetime.time(9,0,0,0,tz.tzlocal())
heure_haut    =  datetime.time(8,30,0,0,tz.tzlocal())
heure_lumiere =  datetime.time(23,30,0,0,tz.tzlocal())
#rint("lecture demarrage arduino!")
serialArduino = serial.Serial('/dev/ttyAMA0', 9600)
#message = serialArduino.readline()
print("initialisation de l'ecran")
lcd = lcddriver.lcd()
lcd.lcd_clear()
print("C'est parti .....!")
velo = Velo()
meteo = dayForecast(0)

while True:
   contenuFich = lireFichier("/sys/bus/w1/devices/28-0000065c6016/w1_slave")
   temperature = recupTemp (contenuFich)
   humi = lireHumTemp().decode()
   #print ("message:", humi)
   lcd.lcd_display_string(lcdLine(velo,16), 1)
   lcd.lcd_display_string(lcdLine("Temp:"+str(temperature)+"      ",16), 2)
   time.sleep(4)
   dateFLumiere = testWeb(dateFLumiere,fichierLumiere,4)
   dateFVolet = testWeb(dateFVolet,fichierVolet,1)
   lcd.lcd_display_string(lcdLine(meteo,16), 1)
   lcd.lcd_display_string(lcdLine("Humi:"+humi[9:-49]+"%     ",16),2)
   time.sleep(4)
   dateFLumiere = testWeb(dateFLumiere,fichierLumiere,4)
   dateFVolet = testWeb(dateFVolet,fichierVolet,1)
   lcd.lcd_display_string(lcdLine(velo,16), 1)
   lcd.lcd_display_string(lcdLine("TempR:"+humi[55:-3]+"     ",16),2) 
   time.sleep(4)
   dateFLumiere = testWeb(dateFLumiere,fichierLumiere,4)
   dateFVolet = testWeb(dateFVolet,fichierVolet,1)
   
   date_now = datetime.datetime.now(tz.tzlocal())
   heure_now = date_now.timetz()
   #print(heure_now)

   goldenr = loc.golden_hour(direction=astral.SUN_RISING, date=datetime.date.today(), local=True)
   today_sr_local = goldenr[0] 

   sun = loc.sun(date=datetime.date.today(), local=True)
   #today_sr_local = sun['dawn']
   today_sunset = sun['sunset']
   #today_ss_local = sun['dusk']

   goldens = loc.golden_hour(direction=astral.SUN_SETTING, date=datetime.date.today(), local=True)
   today_ss_local = goldens[1]


   print('Today at Acigné the sun raised at {} and get down at {} '.format(today_sr_local.strftime('%H:%M'), today_ss_local.strftime('%H:%M')))
   print(str(date_now))
   heure_ss = today_ss_local.timetz()
   heure_sunset =  today_sunset.timetz()
   heure_sr = today_sr_local.timetz()

   if( str(heure_now)[3:-10] == "00"):
      velo = Velo()
      if( heure_now > heure_ss ):
         meteo = dayForecast(1)
      else:
         meteo = dayForecast(0)

   if( heure_now > heure_ss or heure_now < heure_sr ):
      lcd.lcd_display_string(lcdLine("Sun Rise:"+str(heure_sr)[:-3],16),2)
   else:
      lcd.lcd_display_string(lcdLine("Sun Set:"+str(heure_ss)[:-3],16),2) 

   #Jour ou Nuit ?
   if (date_now > today_sr_local and date_now < today_ss_local and jour == False):
       print("Il fait jour!")
       jour = True
   if ((date_now < today_sr_local or date_now > today_ss_local) and jour == True):
       print("Il fait nuit!")
       jour = False
   # Lumiere on
   if( heure_now > heure_sunset and heure_now < heure_lumiere and LUMIERE == False ) :
           print("Allume la lumiere !")
           On("4ON\n")
           LUMIERE = True
   # Lumiere off
   if( (heure_now > heure_lumiere or heure_now < heure_sunset) and LUMIERE == True ) :
           print("Eteint la lumiere !")
           Off("4OFF\n")
           LUMIERE = False
   # Ouveture volet de la chambre
   if( heure_now > heure_chambre and heure_now < heure_ss and jour == True and VOLET_CHAMBRE == False ) :
           print("Ouverure du volet !")
           On("2ON\n")
           LUMIERE = True
           VOLET_CHAMBRE = True
   # Fermeture volet de la chambre
   if( (heure_now < heure_chambre or heure_now > heure_ss) and jour == False and VOLET_CHAMBRE == True ) :
           print("Fermeture du volet !")
           Off("2OFF\n")
           VOLET_CHAMBRE = False

   # Ouveture volet du haut
   if( heure_now > heure_haut and heure_now < heure_ss and jour == True and VOLET_HAUT == False ) :
           print("Ouverure du volet du haut!")
           On("3ON\n")
           VOLET_HAUT = True
   # Fermeture volet du haut
   if( (heure_now < heure_haut or heure_now > heure_ss) and jour == False and VOLET_HAUT == True ) :
           print("Fermeture du volet du haut!")
           Off("3OFF\n")
           VOLET_HAUT = False

   # Ouveture volet du salon
   if( heure_now > heure_sr and heure_now < heure_ss and jour == True and VOLET_SALON == False ) :
           print("Ouverure du volet salon!")
           On("1ON\n")
           VOLET_SALON = True
   # Fermeture volet du salon
   if( (heure_now < heure_sr or heure_now > heure_ss) and jour == False and VOLET_SALON == True ) :
           print("Fermeture du volet salon!")
           Off("1OFF\n")
           VOLET_SALON = False
   print('status L: {} VH: {} VS: {} VC: {}'.format(LUMIERE,VOLET_HAUT,VOLET_SALON,VOLET_CHAMBRE))
   time.sleep(4)
   #2019-10-11 17:05:10.265494+02:00
   lcd.lcd_display_string(lcdLine(str(date_now)[:-16],16),2) 
   time.sleep(4)
   dateFLumiere = testWeb(dateFLumiere,fichierLumiere,4)
   dateFVolet = testWeb(dateFVolet,fichierVolet,1)
